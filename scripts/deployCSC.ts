import hre, { ethers } from 'hardhat';
import fs from 'fs';
import path from 'path';

export async function main() {
  const pathOfCSC = path.resolve(__dirname, '../artifacts/hardhat/contracts/CSC.sol/CSC.json');
  const CSCArtifacts = fs.readFileSync(pathOfCSC, {
    encoding: 'utf-8',
  });
  const CSC = await ethers.deployContract('CSC', ['CSC', '$CSC']);
  await CSC.deployed();
  const pathOfDeployments = path.resolve(__dirname, '../deployments/' + hre.network.name);
  const filePath = path.join(pathOfDeployments, 'CSC.json');
  if (!fs.existsSync(pathOfDeployments)) {
    fs.mkdirSync(pathOfDeployments, { recursive: true });
  }
  fs.writeFileSync(
    filePath,
    JSON.stringify({ address: CSC.address, abi: JSON.parse(CSCArtifacts).abi }),
    { encoding: 'utf-8' }
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
