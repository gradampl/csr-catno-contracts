import hre, { ethers } from 'hardhat';
import fs from 'fs';
import path from 'path';

export async function main() {
  const pathOfStakingLPRewards = path.resolve(
    __dirname,
    '../artifacts/hardhat/contracts/StakingLPRewards.sol/StakingLPRewards.json'
  );
  const StakingLPRewardsArtifacts = fs.readFileSync(pathOfStakingLPRewards, {
    encoding: 'utf-8',
  });

  const pathOfCSC = path.resolve(__dirname, '../deployments/' + hre.network.name);
  const CSCFilePath = path.join(pathOfCSC, 'CSC.json');
  const CSCDeploymentsInfo = JSON.parse(
    fs.readFileSync(CSCFilePath, {
      encoding: 'utf-8',
    })
  );

  const pathOfStakeManager = path.resolve(__dirname, '../deployments/' + hre.network.name);
  const StakeManagerFilePath = path.join(pathOfStakeManager, 'StakeManager.json');
  const StakeManagerDeploymentsInfo = JSON.parse(
    fs.readFileSync(StakeManagerFilePath, {
      encoding: 'utf-8',
    })
  );

  const wallet = new ethers.Wallet(process.env.DEPLOYER_KEY!);

  console.log([
    wallet.publicKey,
    CSCDeploymentsInfo.address,
    CSCDeploymentsInfo.address,
    StakeManagerDeploymentsInfo.address,
  ]);

  const StakingLPRewards = await ethers.deployContract('StakingLPRewards', [
    wallet.publicKey,
    CSCDeploymentsInfo.address,
    CSCDeploymentsInfo.address,
    StakeManagerDeploymentsInfo.address,
  ]);
  await StakingLPRewards.deployed();

  const pathOfDeployments = path.resolve(__dirname, '../deployments/' + hre.network.name);
  const filePath = path.join(pathOfDeployments, 'StakingLPRewards.json');

  if (!fs.existsSync(pathOfDeployments)) {
    fs.mkdirSync(pathOfDeployments, { recursive: true });
  }
  fs.writeFileSync(
    filePath,
    JSON.stringify({
      address: StakingLPRewards.address,
      abi: JSON.parse(StakingLPRewardsArtifacts).abi,
    }),
    { encoding: 'utf-8' }
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
