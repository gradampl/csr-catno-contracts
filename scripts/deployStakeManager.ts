import hre, { ethers } from 'hardhat';
import fs from 'fs';
import path from 'path';

export async function main() {
  const pathOfStakeManager = path.resolve(
    __dirname,
    '../artifacts/hardhat/contracts/StakeManager.sol/StakeManager.json'
  );
  const StakeManagerArtifacts = fs.readFileSync(pathOfStakeManager, {
    encoding: 'utf-8',
  });
  const StakeManager = await ethers.deployContract('StakeManager');
  await StakeManager.deployed();
  const pathOfDeployments = path.resolve(__dirname, '../deployments/' + hre.network.name);
  const filePath = path.join(pathOfDeployments, 'StakeManager.json');
  if (!fs.existsSync(pathOfDeployments)) {
    fs.mkdirSync(pathOfDeployments, { recursive: true });
  }
  fs.writeFileSync(
    filePath,
    JSON.stringify({ address: StakeManager.address, abi: JSON.parse(StakeManagerArtifacts).abi }),
    { encoding: 'utf-8' }
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
