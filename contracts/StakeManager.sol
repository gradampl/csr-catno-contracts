// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

/*
   _                
  | |                ██████  █████  ███    ██ ████████  ██████  
 / __) ___ ___ _ __ ██      ██   ██ ████   ██    ██    ██    ██ 
 \__ \/ __/ __| '__|██      ███████ ██ ██  ██    ██    ██    ██ 
 (   / (__\__ \ |   ██      ██   ██ ██  ██ ██    ██    ██    ██ 
  |_| \___|___/_|    ██████ ██   ██ ██   ████    ██     ██████  
*/

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

struct Stake {
    uint256 amount;
    uint256 timestamp;
    bool isStaking;
}

contract StakeManager is Ownable, ReentrancyGuard {
    uint256 public constant unstakingPeriodCSC = 21 days;
    uint256 public constant unstakingPeriodLP = 14 days;
    uint256 public constant initialFee = 50;
    address[] public whitelistedContracts;

    mapping(address => mapping(address => uint256))
        internal userRewardPerTokenPaid;
    uint256 internal totalSupply;
    mapping(address => Stake) internal stakes;

    function getWhitelistedContracts() public view returns (address[] memory) {
        return whitelistedContracts;
    }

    function setWhiteListedContracts(
        address[] memory _whitelistedContracts
    ) public onlyOwner {
        whitelistedContracts = _whitelistedContracts;
    }

    function getTotalSupply() public view returns (uint256) {
        return totalSupply;
    }

    function setTotalSupply(
        uint newTotalSupply
    ) public onlyWhiteListedContracts {
        totalSupply = newTotalSupply;
    }

    function getUserStake(address adr) public view returns (Stake memory) {
        return stakes[adr];
    }

    function setUserStaking(
        address adr,
        uint256 balance,
        bool isStaking,
        bool withTimestamp
    ) public onlyWhiteListedContracts {
        if (withTimestamp) {
            stakes[adr] = Stake(balance, block.timestamp, isStaking);
        } else {
            stakes[adr] = Stake(balance, stakes[adr].timestamp, isStaking);
        }
    }

    function getUserRewardPerTokenPaid(
        address stakingRewardContract,
        address adr
    ) public view returns (uint256) {
        return userRewardPerTokenPaid[stakingRewardContract][adr];
    }

    function setUserRewardPerTokenPaid(
        address stakingRewardContract,
        address adr,
        uint256 _balance
    ) public onlyWhiteListedContracts {
        userRewardPerTokenPaid[stakingRewardContract][adr] = _balance;
    }

    modifier onlyWhiteListedContracts() {
        bool isIncluded = false;
        address[] memory addresses = getWhitelistedContracts();
        for (uint i = 0; i < addresses.length; i++) {
            if (addresses[i] == msg.sender) {
                isIncluded = true;
                break;
            }
        }
        require(isIncluded, "address not inlcuded");
        _;
    }

    function approve(
        address adr,
        uint256 amount
    ) public nonReentrant onlyWhiteListedContracts {
        ERC20(adr).approve(msg.sender, amount);
    }

    function calculatePenaltyAmount(
        uint256 _stakeDuration,
        uint256 _stakeAmount,
        bool isLPContract
    ) public pure returns (uint256 penaltyAmount) {
        uint256 howManyDays = isLPContract
            ? unstakingPeriodLP
            : unstakingPeriodCSC;
        if (_stakeDuration >= howManyDays) {
            return 0;
        }
        uint256 remainingDays = howManyDays - _stakeDuration;
        uint256 fee = (initialFee * remainingDays) / howManyDays;
        uint256 feeAmount = (_stakeAmount * fee) / 100;
        return feeAmount;
    }
}
