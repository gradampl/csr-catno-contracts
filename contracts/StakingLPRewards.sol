// // SPDX-License-Identifier: UNLICENSED

// pragma solidity ^0.8.17;

// /*
//    _                
//   | |                ██████  █████  ███    ██ ████████  ██████  
//  / __) ___ ___ _ __ ██      ██   ██ ████   ██    ██    ██    ██ 
//  \__ \/ __/ __| '__|██      ███████ ██ ██  ██    ██    ██    ██ 
//  (   / (__\__ \ |   ██      ██   ██ ██  ██ ██    ██    ██    ██ 
//   |_| \___|___/_|    ██████ ██   ██ ██   ████    ██     ██████  
// */

// import "forge-std/console.sol";
// import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
// import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
// import "@openzeppelin/contracts/security/Pausable.sol";
// import "@openzeppelin/contracts/access/Ownable.sol";
// import "@openzeppelin/contracts/utils/math/SafeMath.sol";
// import "./interfaces/IStakingRewards.sol";
// import "./StakeManager.sol";
// import "./CSC.sol";

// contract StakingLPRewards is
//     IStakingRewards,
//     ReentrancyGuard,
//     Pausable,
//     Ownable
// {
//     using SafeMath for uint256;

//     uint256 private constant COLLATERAL_RELEASE_DELAY = 21 days; // Collateral release delay in seconds

//     /* ========== STATE VARIABLES ========== */

//     CSC public rewardsToken;
//     ERC20 public stakingToken;
//     StakeManager stakeManager;
//     uint256 public periodFinish = 0;
//     uint256 public rewardRate = 0;
//     // Total amount of time per period
//     uint256 public rewardsDuration = 30 days;
//     uint256 public lastUpdateTime;
//     uint256 public rewardPerTokenStored;

//     mapping(address => uint256) public rewards;

//     /* ========== CONSTRUCTOR ========== */

//     constructor(
//         address _owner,
//         address _rewardsToken,
//         address _stakingToken,
//         address _stakeManager
//     ) Ownable() {
//         transferOwnership(_owner);
//         rewardsToken = CSC(_rewardsToken);
//         stakingToken = ERC20(_stakingToken);
//         stakeManager = StakeManager(_stakeManager);
//     }

//     /* ========== VIEWS ========== */

//     function totalSupply() external view returns (uint256) {
//         return stakeManager.getTotalSupply();
//     }

//     function balanceOf(address account) external view returns (uint256) {
//         return stakeManager.getUserStake(account).amount;
//     }

//     function lastTimeRewardApplicable() public view returns (uint256) {
//         return block.timestamp < periodFinish ? block.timestamp : periodFinish;
//     }

//     function rewardPerToken() public view returns (uint256) {
//         if (stakeManager.getTotalSupply() == 0) {
//             return rewardPerTokenStored;
//         }
//         return
//             rewardPerTokenStored.add(
//                 (lastTimeRewardApplicable() - lastUpdateTime)
//                     .mul(rewardRate)
//                     .mul(1e18)
//                     .div(stakeManager.getTotalSupply())
//             );
//     }

//     function earned(address account) public view returns (uint256) {
//         return
//             stakeManager
//                 .getUserStake(account)
//                 .amount
//                 .mul(
//                     rewardPerToken().sub(
//                         stakeManager.getUserRewardPerTokenPaid(
//                             address(this),
//                             account
//                         )
//                     )
//                 )
//                 .div(1e18)
//                 .add(rewards[account]);
//     }

//     function getRewardForDuration() external view returns (uint256) {
//         return rewardRate.mul(rewardsDuration);
//     }

//     /* ========== MUTATIVE FUNCTIONS ========== */

//     function stake(
//         uint256 amount
//     ) external nonReentrant whenNotPaused updateReward(msg.sender) {
//         require(amount > 0, "Cannot stake 0");
//         stakeManager.setTotalSupply(stakeManager.getTotalSupply().add(amount));
//         stakeManager.setUserStaking(
//             msg.sender,
//             stakeManager.getUserStake(msg.sender).amount + amount,
//             true,
//             true
//         );
//         stakingToken.transferFrom(msg.sender, address(stakeManager), amount);
//         emit Staked(msg.sender, amount);
//     }

//     function withdraw(
//         uint256 amount
//     ) public nonReentrant updateReward(msg.sender) {
//         require(amount > 0, "Cannot withdraw 0");
//         stakeManager.setTotalSupply(stakeManager.getTotalSupply().sub(amount));
//         stakeManager.approve((address(stakingToken)), amount);
//         stakingToken.transferFrom(address(stakeManager), msg.sender, amount);
//         bool isStillStaking = stakeManager.getUserStake(msg.sender).amount -
//             amount >
//             0;
//         stakeManager.setUserStaking(
//             msg.sender,
//             stakeManager.getUserStake(msg.sender).amount - amount,
//             false,
//             isStillStaking
//         );
//         emit Withdrawn(msg.sender, amount);
//     }

//     function getReward() public nonReentrant updateReward(msg.sender) {
//         uint256 reward = rewards[msg.sender];
//         if (reward > 0) {
//             Stake memory userStake = stakeManager.getUserStake(msg.sender);
//             require(!userStake.isStaking, "can't claim rewards while staking");
//             // Calculate the stake duration
//             uint256 stakeDuration = block.timestamp - userStake.timestamp;

//             if (stakeDuration >= COLLATERAL_RELEASE_DELAY) {
//                 rewards[msg.sender] = 0;
//                 rewardsToken.transfer(msg.sender, reward);
//             } else {
//                 // Calculate the penalty amount based on the stake duration
//                 uint256 penaltyAmount = stakeManager.calculatePenaltyAmount(
//                     stakeDuration,
//                     reward,
//                     true
//                 );
//                 uint256 rewardPayout = reward - penaltyAmount;
//                 rewards[msg.sender] = 0;
//                 rewardsToken.transfer(address(0), penaltyAmount);
//                 rewardsToken.transfer(msg.sender, rewardPayout);
//                 emit RewardPaid(msg.sender, reward);
//             }
//         }
//     }

//     /* ========== RESTRICTED FUNCTIONS ========== */

//     function notifyRewardAmount(
//         uint256 reward
//     ) external onlyOwner updateReward(address(0)) {
//         if (block.timestamp >= periodFinish) {
//             rewardRate = reward.div(rewardsDuration);
//         } else {
//             uint256 remaining = periodFinish.sub(block.timestamp);
//             uint256 leftover = remaining.mul(rewardRate);
//             rewardRate = reward.add(leftover).div(rewardsDuration);
//         }

//         // Ensure the provided reward amount is not more than the balance in the contract.
//         // This keeps the reward rate in the right range, preventing overflows due to
//         // very high values of rewardRate in the earned and rewardsPerToken functions;
//         // Reward + leftover must be less than 2^256 / 10^18 to avoid overflow.
//         uint balance = rewardsToken.balanceOf(address(this));
//         require(
//             rewardRate <= balance.div(rewardsDuration),
//             "Provided reward too high"
//         );

//         lastUpdateTime = block.timestamp;
//         periodFinish = block.timestamp.add(rewardsDuration);
//         emit RewardAdded(reward);
//     }

//     function recoverERC20(
//         address tokenAddress,
//         uint256 tokenAmount
//     ) external onlyOwner {
//         ERC20(tokenAddress).transfer(owner(), tokenAmount);
//         emit Recovered(tokenAddress, tokenAmount);
//     }

//     function setRewardsDuration(uint256 _rewardsDuration) external onlyOwner {
//         require(
//             block.timestamp > periodFinish,
//             "Previous rewards period must be complete before changing the duration for the new period"
//         );
//         rewardsDuration = _rewardsDuration;
//         emit RewardsDurationUpdated(rewardsDuration);
//     }

//     function pauseContract() external onlyOwner {
//         _pause();
//     }

//     function unpauseContract() external onlyOwner {
//         _unpause();
//     }

//     /* ========== MODIFIERS ========== */

//     modifier updateReward(address account) {
//         rewardPerTokenStored = rewardPerToken();
//         lastUpdateTime = lastTimeRewardApplicable();
//         if (account != address(0)) {
//             rewards[account] = earned(account);
//             uint256 rewardPerTokenPaid = stakeManager.getUserRewardPerTokenPaid(
//                 address(this),
//                 account
//             );

//             uint256 newRewardPerTokenPaid = rewardPerTokenStored +
//                 rewardPerTokenPaid;
//             stakeManager.setUserRewardPerTokenPaid(
//                 address(this),
//                 account,
//                 newRewardPerTokenPaid
//             );
//         }
//         _;
//     }

//     /* ========== EVENTS ========== */

//     event RewardAdded(uint256 reward);
//     event Staked(address indexed user, uint256 amount);
//     event Withdrawn(address indexed user, uint256 amount);
//     event RewardPaid(address indexed user, uint256 reward);
//     event RewardsDurationUpdated(uint256 newDuration);
//     event Recovered(address token, uint256 amount);
// }
