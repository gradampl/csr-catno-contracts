import { ethers } from 'hardhat';
import { Contract, ContractFactory } from 'ethers';
import { expect } from 'chai';
import { afterEachFn } from './utils';
import { main } from '../../scripts/deployAll';

describe('Admin and Manager Roles', function () {
  let net: any;
  let csrCanto: Contract;

  beforeEach(async function () {
    net = await main();
    const CsrCanto: ContractFactory = await ethers.getContractFactory('CsrCanto');
    csrCanto = CsrCanto.attach(net.contracts.csrCanto);

    csrCanto.connect(net.signers.admin).a_setManagerRole(net.signers.manager.address);
  });

  this.afterEach(() => afterEachFn(csrCanto));

  it('should set the ADMIN_ROLE correctly', async function () {
    await expect(csrCanto.connect(net.signers.admin).a_setAdminRole(net.signers.alice.address)).to
      .be.fulfilled;
    expect(await csrCanto.ADMIN_ROLE()).to.equal(net.signers.alice.address);
  });

  it('should set the MANAGER_ROLE correctly', async function () {
    await expect(csrCanto.connect(net.signers.admin).a_setManagerRole(net.signers.manager.address))
      .to.be.fulfilled;
    expect(await csrCanto.MANAGER_ROLE()).to.equal(net.signers.manager.address);
  });

  it('should allow only the MANAGER_ROLE to add and remove claimers', async function () {
    // Alice wraps 100 $CANTO
    await csrCanto.connect(net.signers.alice).deposit({ value: 100 });
    expect(await csrCanto.claimersTotalSupply()).to.equal(0);

    // Manager adds a new claimer
    await expect(
      csrCanto
        .connect(net.signers.manager)
        .m_addClaimer(net.signers.alice.address, net.signers.alice.address)
    ).to.be.fulfilled;
    expect(await csrCanto.claimersTotalSupply()).to.equal(100);
    expect(await csrCanto.isClaimer(net.signers.alice.address)).to.equal(true);

    // Manager removes a claimer
    await expect(csrCanto.connect(net.signers.manager).m_delClaimer(net.signers.alice.address)).to
      .be.fulfilled;
    expect(await csrCanto.claimersTotalSupply()).to.equal(0);
    expect(await csrCanto.isClaimer(net.signers.alice.address)).to.equal(false);
  });
});
