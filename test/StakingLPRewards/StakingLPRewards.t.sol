// pragma solidity 0.8.17;

// import "forge-std/Test.sol";
// import "forge-std/console.sol";
// import "../../contracts/CSC.sol";
// import "../../contracts/StakingLPRewards.sol";
// import "../../contracts/StakeManager.sol";
// import "../../contracts/mocks/ERC20Mock.sol";

// contract StakingLPRewardsTest is Test {
//     CSC csc;
//     StakingLPRewards sr;
//     StakeManager sm;
//     ERC20Mock mockLPToken;
//     address[] srAddresses;

//     address vitalik = 0xd8dA6BF26964aF9D7eEd9e03E53415D37aA96045;
//     uint DAY = 86400;

//     uint256[] rewards = [
//         500000 ether,
//         375000 ether,
//         281250 ether,
//         210938 ether,
//         158203 ether,
//         118652 ether,
//         88989 ether,
//         66742 ether,
//         50056 ether,
//         37542 ether,
//         28157 ether,
//         21118 ether,
//         15838 ether,
//         11879 ether,
//         8909 ether,
//         6682 ether,
//         5011 ether,
//         3758 ether,
//         2819 ether,
//         2114 ether,
//         1586 ether,
//         1189 ether,
//         892 ether,
//         669 ether,
//         502 ether,
//         376 ether,
//         282 ether,
//         212 ether,
//         159 ether,
//         119 ether,
//         89 ether,
//         67 ether,
//         50 ether,
//         38 ether,
//         28 ether,
//         21 ether,
//         16 ether,
//         12 ether,
//         9 ether,
//         7 ether,
//         5 ether,
//         4 ether,
//         3 ether,
//         2 ether,
//         2 ether,
//         1 ether,
//         1 ether
//     ];

//     event Recovered(address, uint);
//     event Transfer(address indexed from, address indexed to, uint256 reward);

//     function setUp() public {
//         csc = new CSC("CSC", "$CSC", address(this), 10_000_000 ether);
//         mockLPToken = new ERC20Mock("Mock", "$Mock");
//         sm = new StakeManager();
//         sr = new StakingLPRewards(
//             address(this),
//             address(csc),
//             address(mockLPToken),
//             address(sm)
//         );
//         srAddresses.push(address(sr));
//         sm.setWhiteListedContracts(srAddresses);
//     }

//     function setRewards(bool withWarping) internal {
//         if (withWarping) {
//             vm.warp(block.timestamp + DAY);
//         }
//         csc.transfer(address(sr), 500_000 ether);
//         sr.notifyRewardAmount(500_000 ether);
//     }

//     function setSpecificRewards(bool withWarping, uint _rewards) internal {
//         if (withWarping) {
//             vm.warp(block.timestamp + DAY);
//         }
//         sr.notifyRewardAmount(_rewards);
//     }

//     function test_ConstructorSetsRewardsToken() public {
//         assertEq(address(sr.rewardsToken()), address(csc));
//     }

//     function test_ConstructorSetsStakingToken() public {
//         assertEq(address(sr.stakingToken()), address(mockLPToken));
//     }

//     function test_ConstructorSetsOwner() public {
//         assertEq(sr.owner(), address(this));
//     }

//     function test_TotalSupplyIsNotZero() public {
//         assertEq(sr.totalSupply(), 0 ether);
//     }

//     function test_OnlyOwnerCanCallNotifyRewardAmountRevert() public {
//         vm.prank(address(csc));
//         vm.expectRevert("Ownable: caller is not the owner");
//         sr.notifyRewardAmount(500_000 ether);
//     }

//     function test_OnlyOwnerCanCallSetRewardsDuration() public {
//         sr.setRewardsDuration(30 days);
//         assertEq(sr.rewardsDuration(), 30 days);
//     }

//     function test_OnlyOwnerCanCallSetRewardsDurationRevert() public {
//         vm.prank(address(csc));
//         vm.expectRevert("Ownable: caller is not the owner");
//         sr.setRewardsDuration(30 days);
//     }

//     function test_OnlyOwnerCanPauseContractRevert() public {
//         vm.prank(address(csc));
//         vm.expectRevert("Ownable: caller is not the owner");
//         sr.pauseContract();
//     }

//     function test_OnlyOwnerCanUnPauseContractRevert() public {
//         vm.prank(address(csc));
//         vm.expectRevert("Ownable: caller is not the owner");
//         sr.unpauseContract();
//     }

//     function test_OnlyOwnerCanPauseContract() public {
//         sr.pauseContract();
//         assertEq(sr.paused(), true);
//     }

//     function test_OnlyOwnerCanUnPauseContract() public {
//         sr.pauseContract();
//         sr.unpauseContract();
//         assertEq(sr.paused(), false);
//     }

//     function test_OnlyOwnerCanRecoverERC20Tokens() public {
//         csc.transfer(address(sr), 500_001 ether);
//         vm.expectEmit(address(sr));
//         emit Recovered(address(csc), 500_001 ether);
//         sr.recoverERC20(address(csc), 500_001 ether);
//     }

//     function test_OnlyOwnerCanRecoverERC20TokensRevert() public {
//         vm.prank(address(csc));
//         vm.expectRevert("Ownable: caller is not the owner");
//         sr.recoverERC20(address(csc), 10 ether);
//     }

//     function test_LastTimeRewardApplicableShouldReturnZero() public {
//         assertEq(sr.lastTimeRewardApplicable(), 0);
//     }

//     function test_WhenUserStakesTotalSupplyShouldIncrease() public {
//         mockLPToken.mint(address(this), 99 ether);
//         mockLPToken.approve(address(sr), 99 ether);
//         sr.stake(99 ether);
//         assertEq(sr.totalSupply(), 99 ether);
//     }

//     function test_WhenRewardsAreUpdatedLastTimeRewardApplicableShouldEqualCurrentTime()
//         public
//     {
//         setRewards(true);
//         assertEq(sr.lastTimeRewardApplicable(), block.timestamp);
//     }

//     function test_RewardPerTokenShouldReturnZeroWhenNoRewardsAreSet() public {
//         assertEq(sr.rewardPerToken(), 0);
//     }

//     function test_RewardsPerTokenShouldInreaseAfterADay() public {
//         setRewards(true);
//         mockLPToken.mint(address(this), 99 ether);
//         mockLPToken.approve(address(sr), 99 ether);
//         sr.stake(99 ether);
//         vm.warp(block.timestamp + DAY);
//         assertEq(sr.rewardPerToken() > 0, true);
//     }

//     function test_EarnedShouldReturnZeroWhenNotStaked() public {
//         assertEq(sr.earned(address(this)), 0);
//     }

//     function test_EarnedShouldReturnAboveZeroWhenStaked() public {
//         setRewards(false);
//         mockLPToken.mint(address(this), 99 ether);
//         mockLPToken.approve(address(sr), 99 ether);
//         sr.stake(1 ether);
//         sr.getReward();
//         vm.warp(DAY);
//         assertEq(sr.earned(address(this)) > 0, true);
//     }

//     function test_WithdrawReturnsAllStakeCollateralEvenIfWithingSlasingPeriod()
//         public
//     {
//         mockLPToken.mint(address(this), 1 ether);
//         mockLPToken.approve(address(sr), 1 ether);
//         sr.stake(1 ether);
//         vm.warp(DAY + 1);
//         sr.withdraw(1 ether);
//         assertEq(mockLPToken.balanceOf(address(this)), 1 ether);
//     }

//     function test_AllMonthsSingleStakingReward() public {
//         mockLPToken.mint(address(this), 99 ether);
//         uint256 totalEmitted;
//         for (uint i = 0; i < 1; i++) {
//             sr = new StakingLPRewards(
//                 address(this),
//                 address(csc),
//                 address(mockLPToken),
//                 address(sm)
//             );

//             csc.transfer(address(sr), rewards[i]);
//             srAddresses.push(address(sr));
//             sm.setWhiteListedContracts(srAddresses);
//             sr.notifyRewardAmount(rewards[i]);
//             mockLPToken.approve(address(sr), 1 ether);
//             sr.stake(1 ether);
//             vm.warp(block.timestamp + DAY * 30 + 2);
//             assertEq(block.timestamp > sr.periodFinish(), true);
//             if (rewards[i] == 0) {
//                 sr.withdraw(1 ether);
//                 // we expect all the stake tokens to come back but the rewards token are getting slashed accordingly
//                 totalEmitted += rewards[i];
//                 assertEq(
//                     csc.balanceOf(address(this)) > rewards[i] &&
//                         csc.balanceOf(address(this)) < rewards[i] + 1 ether,
//                     true
//                 );
//             } else {
//                 totalEmitted += rewards[i];
//                 vm.expectEmit(true, true, false, false);
//                 emit Transfer(address(sr), address(this), 0);
//                 sr.withdraw(1 ether);
//                 assertEq(
//                     csc.balanceOf(address(this)) > totalEmitted - 1 ether &&
//                         csc.balanceOf(address(this)) <
//                         totalEmitted + 1 ether + 10_000_000 ether,
//                     true
//                 );
//             }
//         }
//     }

//     function test_UserGetsSlashedWhenExitingAfterADay() public {
//         mockLPToken.mint(address(this), 99 ether);
//         csc.transfer(address(sr), 500_000 ether);
//         sr.notifyRewardAmount(500_000 ether);
//         mockLPToken.approve(address(sr), 1 ether);
//         sr.stake(1 ether);
//         vm.warp(block.timestamp + 1);
//         sr.withdraw(1 ether);
//         assertEq(mockLPToken.balanceOf(address(this)), 99 ether);
//         assertEq(csc.balanceOf(address(this)) < 952_000_000 ether, true);
//         assertEq(
//             csc.balanceOf(address(0)) > 7500 ether &&
//                 csc.balanceOf(address(0)) < 8400 ether,
//             true
//         );
//     }
// }
