pragma solidity 0.8.17;

import "forge-std/Test.sol";
import "../../contracts/CSC.sol";

contract CSCTest is Test {
    CSC csc;
    address vitalik = 0xd8dA6BF26964aF9D7eEd9e03E53415D37aA96045;

    function setUp() public {
        csc = new CSC("CSC", "$CSC", address(this), 10_000_000 ether);
    }

    function test_TotalSupplyIs10Million() public {
        assertEq(csc.totalSupply(), 10_000_000 ether);
    }
}
