pragma solidity 0.8.17;

import "forge-std/Test.sol";
import "forge-std/console.sol";
import "../../contracts/CSC.sol";
import "../../contracts/StakingCSCRewards.sol";
import "../../contracts/StakeManager.sol";
import "../../contracts/mocks/ERC20Mock.sol";

contract StakingCSCRewardsTest is Test {
    CSC csc;
    StakingCSCRewards sr;
    StakeManager sm;
    address[] srAddresses;

    address vitalik = 0xd8dA6BF26964aF9D7eEd9e03E53415D37aA96045;

    function setUp() public {
        csc = new CSC("CSC", "$CSC", address(this), 10_000_000 ether);
        sm = new StakeManager();
        sr = new StakingCSCRewards(address(this), address(csc), address(sm));
        srAddresses.push(address(sr));
        sm.setWhiteListedContracts(srAddresses);
    }

    function test_CanNotCallSetterFunctionOnStakeManagerContract() public {
        vm.prank(vitalik);
        vm.expectRevert("Ownable: caller is not the owner");
        sm.setWhiteListedContracts(srAddresses);
        vm.expectRevert("address not inlcuded");
        sm.setTotalSupply(1 ether);
        vm.expectRevert("address not inlcuded");
        sm.setUserStaking(vitalik, 0, true, false);
        vm.expectRevert("address not inlcuded");
        sm.setUserRewardPerTokenPaid(vitalik, vitalik, 0 ether);
        vm.expectRevert("address not inlcuded");
        sm.approve(vitalik, 1 ether);
    }
}
