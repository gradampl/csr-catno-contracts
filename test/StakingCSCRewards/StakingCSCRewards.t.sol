pragma solidity 0.8.17;

import "forge-std/Test.sol";
import "forge-std/console.sol";
import "../../contracts/CSC.sol";
import "../../contracts/StakingCSCRewards.sol";
import "../../contracts/StakeManager.sol";
import "../../contracts/mocks/ERC20Mock.sol";

contract StakingCSCRewardsTest is Test {
    CSC csc;
    StakingCSCRewards sr;
    StakeManager sm;
    address[] srAddresses;

    address vitalik = 0xd8dA6BF26964aF9D7eEd9e03E53415D37aA96045;
    uint DAY = 86400;

    uint256[] rewards = [
        500000 ether,
        375000 ether,
        281250 ether,
        210938 ether,
        158203 ether,
        118652 ether,
        88989 ether,
        66742 ether,
        50056 ether,
        37542 ether,
        28157 ether,
        21118 ether,
        15838 ether,
        11879 ether,
        8909 ether,
        6682 ether,
        5011 ether,
        3758 ether,
        2819 ether,
        2114 ether,
        1586 ether,
        1189 ether,
        892 ether,
        669 ether,
        502 ether,
        376 ether,
        282 ether,
        212 ether,
        159 ether,
        119 ether,
        89 ether,
        67 ether,
        50 ether,
        38 ether,
        28 ether,
        21 ether,
        16 ether,
        12 ether,
        9 ether,
        7 ether,
        5 ether,
        4 ether,
        3 ether,
        2 ether,
        2 ether,
        1 ether,
        1 ether
    ];

    event Recovered(address, uint);
    event Transfer(address indexed from, address indexed to, uint256 reward);
    event RewardPaid(address, uint256);

    function setUp() public {
        csc = new CSC("CSC", "$CSC", address(this), 10_000_000 ether);
        sm = new StakeManager();
        sr = new StakingCSCRewards(address(this), address(csc), address(sm));
        srAddresses.push(address(sr));
        sm.setWhiteListedContracts(srAddresses);
    }

    function test_ConstructorSetsRewardsAndStakingToken() public {
        assertEq(address(sr.rewardsAndStakingToken()), address(csc));
    }

    function test_ConstructorSetsOwner() public {
        assertEq(sr.owner(), address(this));
    }

    function test_TotalSupplyIsNotZero() public {
        assertEq(sr.totalSupply(), 0 ether);
    }

    function test_OnlyOwnerCanCallNotifyRewardAmountRevert() public {
        vm.prank(address(csc));
        vm.expectRevert("Ownable: caller is not the owner");
        sr.notifyRewardAmount(500_000 ether);
    }

    function test_OnlyOwnerCanCallSetRewardsDuration() public {
        sr.setRewardsDuration(10 days);
        assertEq(sr.rewardsDuration(), 10 days);
    }

    function test_OnlyOwnerCanCallSetRewardsDurationRevert() public {
        vm.prank(address(csc));
        vm.expectRevert("Ownable: caller is not the owner");
        sr.setRewardsDuration(10 days);
    }

    function test_OnlyOwnerCanPauseContractRevert() public {
        vm.prank(address(csc));
        vm.expectRevert("Ownable: caller is not the owner");
        sr.pauseContract();
    }

    function test_OnlyOwnerCanUnPauseContractRevert() public {
        vm.prank(address(csc));
        vm.expectRevert("Ownable: caller is not the owner");
        sr.unpauseContract();
    }

    function test_OnlyOwnerCanPauseContract() public {
        sr.pauseContract();
        assertEq(sr.paused(), true);
    }

    function test_OnlyOwnerCanUnPauseContract() public {
        sr.pauseContract();
        sr.unpauseContract();
        assertEq(sr.paused(), false);
    }

    function test_OnlyOwnerCanRecoverERC20Tokens() public {
        csc.transfer(address(sr), 500_001 ether);
        vm.expectEmit(true, true, false, true);
        emit Recovered(address(csc), 500_001 ether);
        sr.recoverERC20(address(csc), 500_001 ether);
    }

    function test_OnlyOwnerCanRecoverERC20TokensRevert() public {
        vm.prank(address(csc));
        vm.expectRevert("Ownable: caller is not the owner");
        sr.recoverERC20(address(csc), 10 ether);
    }

    function test_LastTimeRewardApplicableShouldReturnZero() public {
        assertEq(sr.lastTimeRewardApplicable(), 0);
    }

    function test_WhenUserStakesTotalSupplyShouldIncrease() public {
        csc.approve(address(sr), 99 ether);
        sr.stake(99 ether);
        assertEq(sr.totalSupply(), 99 ether);
    }

    function test_WhenRewardsAreUpdatedLastTimeRewardApplicableShouldEqualCurrentTime()
        public
    {
        vm.warp(DAY);
        csc.transfer(address(sr), 10_000_000 ether);
        sr.notifyRewardAmount(500_000 ether);
        assertEq(sr.lastTimeRewardApplicable(), DAY);
    }

    function test_RewardPerTokenShouldReturnZeroWhenNoRewardsAreSet() public {
        assertEq(sr.rewardPerToken(), 0);
    }

    function test_RewardsPerTokenShouldInreaseAfterADay() public {
        csc.transfer(address(sr), 500_000 ether);
        sr.notifyRewardAmount(500_000 ether);
        csc.approve(address(sr), 99 ether);
        sr.stake(99 ether);
        vm.warp(block.timestamp + DAY);
        assertEq(sr.rewardPerToken() > 0, true);
    }

    function test_EarnedShouldReturnZeroWhenNotStaked() public {
        assertEq(sr.earned(address(this)), 0);
    }

    function test_EarnedShouldReturnAboveZeroWhenStaked() public {
        csc.transfer(address(sr), 500_000 ether);
        sr.notifyRewardAmount(500_000 ether);
        csc.approve(address(sr), 99 ether);
        sr.stake(1 ether);
        vm.warp(DAY);
        assertEq(
            sr.earned(address(this)) > 16_600 ether &&
                sr.earned(address(this)) < 17_000 ether,
            true
        );
    }

    function test_WithdrawReturnsAllStakeCollateralEvenIfWithingSlasingPeriod()
        public
    {
        csc.approve(address(sr), 1 ether);
        sr.stake(1 ether);
        vm.warp(DAY + 1);
        sr.withdraw(1 ether);
        assertEq(csc.balanceOf(address(this)) == 10_000_000 ether, true);
    }

    function test_AllMonthsSingleStakerStakingReward() public {
        for (uint i = 0; i < 1; i++) {
            sr = new StakingCSCRewards(
                address(this),
                address(csc),
                address(sm)
            );
            csc.transfer(address(sr), rewards[i]);
            srAddresses.push(address(sr));
            sm.setWhiteListedContracts(srAddresses);
            sr.notifyRewardAmount(rewards[i]);
            csc.approve(address(sr), 1 ether);
            sr.stake(1 ether);
            vm.warp(block.timestamp + DAY * 30 + 2);
            assertEq(block.timestamp > sr.periodFinish(), true);
            vm.expectRevert("can not claim rewards while staking");
            sr.getReward();
            sr.withdraw(1 ether);
            vm.warp(block.timestamp + DAY * 30 + 2);
            sr.getReward();
            assertEq(
                (csc.balanceOf(address(this)) > 9_999_999 ether &&
                    csc.balanceOf(address(this)) <= 10_000_000 ether),
                true
            );
        }
    }

    function test_UserGetsSlashedWhenExitingAfterADay() public {
        csc.transfer(address(sr), 500_000 ether);
        sr.notifyRewardAmount(500_000 ether);
        csc.approve(address(sr), 1 ether);
        sr.stake(1 ether);
        vm.warp(block.timestamp + 1);
        sr.withdraw(1 ether);
        assertEq(csc.balanceOf(address(this)), 9_500_000 ether);
        sr.getReward();
        assertEq(csc.balanceOf(address(this)), 9500000098379629629629630);
        assertEq(csc.balanceOf(address(0)), 94521604938271604);
    }

    function test_CanNotWithdrawMoreThanStaked() public {
        csc.transfer(address(sr), 500_000 ether);
        sr.notifyRewardAmount(500_000 ether);
        csc.approve(address(sr), 1 ether);
        sr.stake(1 ether);
        vm.expectRevert(stdError.arithmeticError);
        sr.withdraw(2 ether);
    }

    // function test_AllMonthsManyStakersStakingRewards() public {
    //     for (uint i = 0; i < 1; i++) {
    //         sr = new StakingCSCRewards(
    //             address(this),
    //             address(csc),
    //             address(sm)
    //         );
    //         csc.transfer(address(sr), rewards[i]);
    //         srAddresses.push(address(sr));
    //         sm.setWhiteListedContracts(srAddresses);
    //         sr.notifyRewardAmount(rewards[i]);
    //         csc.approve(address(sr), 1 ether);
    //         uint256 countdown = i;
    //         while (countdown != 0) {
    //             sr.stake(1 ether);
    //             countdown--;
    //         }
    //         vm.warp(block.timestamp + DAY * 30 + 2);
    //         assertEq(block.timestamp > sr.periodFinish(), true);
    //         vm.expectRevert("can not claim rewards while staking");
    //         sr.getReward();
    //         sr.withdraw(1 ether);
    //         vm.warp(block.timestamp + DAY * 30 + 2);
    //         sr.getReward();
    //         assertEq(
    //             (csc.balanceOf(address(this)) > 9_999_999 ether &&
    //                 csc.balanceOf(address(this)) <= 10_000_000 ether),
    //             true
    //         );
    //     }
    // }
}
